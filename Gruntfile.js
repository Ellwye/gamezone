module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'), // mon-super-projet
    watch: {
      sass: {
        files: 'assets/src/css/*.scss',
        tasks: ['sass']
      }
    },
    sass: {
      dist: {
        options: {
          style: 'compressed', // Can be nested, compact, compressed, expanded.
          compass: false
        },
        files: { 
          'assets/css/styles.min.css': 'assets/src/css/styles.scss',
          'assets/css/header.min.css': 'assets/src/css/header.scss',
          'assets/css/footer.min.css': 'assets/src/css/footer.scss',
          'assets/css/commun.min.css': 'assets/src/css/commun.scss',
          'assets/css/carousel.min.css': 'assets/src/css/carousel.scss',
          'assets/css/le-parc.min.css': 'assets/src/css/le-parc.scss',
          'assets/css/plan.min.css': 'assets/src/css/plan.scss',
        }
      }
	  },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: ['assets/src/js/libs/*.js', 'assets/src/js/*.js'],
        dest: 'assets/js/script.min.js'
      }
    },
    imagemin: {
      dynamic: {
          files: [{
              expand: true,
              cwd: 'assets/src/img/',
              src: ['**/*.{png,jpg,gif}'],
              dest: 'assets/img/'
          }]
      }
    },
    clean: ['assets/js', 'assets/css']
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-watch')

  // Default task(s).
  grunt.registerTask('default', ['clean', 'sass', 'uglify', 'imagemin']);

};
