<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 footer-section">
                <ul>
                    <li><a href="#">À propos du parc</a></li>
                    <li><a href="#">Dans la presse</a></li>
                    <li><a href="#">On recrute</a></li>
                    <li><a href="#">Nous contacter</a></li>
                    <li><a href="#">Conditions de vente</a></li>
                    <li><a href="#">Conditions légales</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 horaires footer-section">
                <h4>Horaires d'ouverture</h4>
                <ul>
                    <li id="day"></li>
                    <li>Du lundi au jeudi : 9h-19h</li>
                    <li>Du vendredi au samedi : 9h-20h</li>
                    <li>Le dimanche : 9h – 18h</li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 footer-section">
                <h4>Suivez-nous</h4>
                <div class="rs">
                    <div class="img-rs">
                        <a href="#"><i class='fa fa-facebook'></i></a>
                    </div>
                    <div class="img-rs">
                        <a href="#"><i class='fab fa-instagram'></i></a>
                    </div>
                    <div class="img-rs">
                        <a href="#"><i class='fab fa-twitter'></i></a>
                    </div>
                    <div class="img-rs">
                        <a href="#"><i class='fab fa-youtube'></i></a>
                    </div>
                </div>
                <h4>Besoin d'aide ?</h4>
                <p class="tel-aide">Appelez le 08 59 62 08 59</p>
            </div>
        </div>
    </div>
    <div class="col-12 copyright">
        <p>&copy Tous droits réservés GameZone</p>
    </div>
</footer>