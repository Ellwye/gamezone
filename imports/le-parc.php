<main>
    <section id="carousel">
        <div id="carousel_control" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="assets/img/trace-3157431.jpg" class="d-block w-100"
                        alt="image d'un visage informatisé">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/roller-coaster-2461721.jpg" class="d-block w-100"
                        alt="attraction phare du parc">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/vr-3559837.jpg" class="d-block w-100"
                        alt="personne avec un casque de réalité virtuelle">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carousel_control" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel_control" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="title-container">
            <div class="title">
                <div class="title-contenu">
                    <h1>ouverture septembre 2020</h1>
                    <p class="subtitle">Le premier parc d’attraction au monde
                        entièrement dédié à l’univers des jeux-vidéo</p>
                </div>
                <div class="title-shadow"></div>
            </div>
        </div>
    </section>

    <section id="presentation-parc">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Le parc</h2>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-8 col-md-6 col-sm-12">
                    <div class="img-parc">
                        <img src="assets/img/valencia-4377331.jpg" class="image-responsive"
                            alt="photographie du parc">
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <p class="presentation">
                        GEEK CYBERCENTER est une SAS au capital de 45 824
                        123 euros. Il s’agit d’un groupe possédant plusieurs parcs
                        d’attractions dans le monde comme : BATTLE KART VR,
                        FORTNITE ADVENTURE, PUBG SURVIVOR.
                        En septembre 2019, le groupe ouvrira un nouveau parc :
                        GAME ZONE. Ce dernier sera le premier parc d’attraction
                        au monde entièrement dédié à l’univers des jeux-vidéo.
                    </p>
                </div>
            </div>
            <div class="col-12 fond-infos">
                <div class="row justify-content-center">
                    <div class="col-lg-3 col-md-6 col-sm-12 info">
                        <p class="number">
                            9
                        </p>
                        <p class="number-subtitle">
                            attractions
                        </p>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 info">
                        <p class="number">
                            5
                        </p>
                        <p class="number-subtitle">
                            restaurants
                        </p>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 info">
                        <p class="number">
                            500k
                        </p>
                        <p class="number-subtitle">
                            visiteurs
                        </p>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 info">
                        <p class="number">
                            900
                        </p>
                        <p class="number-subtitle">
                            hectares
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="attractions-phares">
        <div class="container">
            <div class="row justify-content-center align-items-center align-cards-attraction">
                <div class="col-12">
                    <h2>Nos attractions phares</h2>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-8 col-12">
                    <div class="card-attration">
                        <div class="img-attraction">
                            <img src="assets/img/attractions/logo_game_center.png" class="image-responsive" alt="logotype attraction game center">
                        </div>
                        <h3>Gain XP : 500 XP / partie</h3>
                        <p class="accessible-info">
                            Accessible à tous
                        </p>
                        <p class="infos-generales">
                            Un espace de 3000 m2 dédié au Retro Gaming. Vous retrouverez toute l’ambiance des salles d’arcade des années 80 avec les bornes de l’époque pour défier vos amis dans les meilleurs jeux retro : Mario, Centipede, Q Bert, Space invaders, Pac-Man
                        </p>
                        <div class="attraction-color attraction-color-1"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-8 col-12">
                    <div class="card-attration">
                        <div class="img-attraction">
                            <img src="assets/img/attractions/logo_BATTLE_KART.png" class="image-responsive" alt="logotype attraction battle kart">
                        </div>
                        <h3>Gain XP : 1000 XP / partie</h3>
                        <p class="accessible-info">
                            Accessible à tous
                        </p>
                        <p class="infos-generales">
                            BattleKart, c’est la quintessence du jeu vidéo, de la réalité augmentée et du karting électrique, réunis pour vous procurer des sensations inédites entre amis, en familles ou entre collègues ! C’est avant tout l’un des plus grands écrans de cinéma
                        </p>
                        <div class="attraction-color attraction-color-1"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-8 col-12">
                    <div class="card-attration">
                        <div class="img-attraction">
                            <img src="assets/img/attractions/logo_contagion_VR.png" class="image-responsive" alt="logotype attraction game center">
                        </div>
                        <h3>Gain XP : 1500 XP / partie</h3>
                        <p class="accessible-info">
                            Interdit aux moins d'1m30
                        </p>
                        <p class="infos-generales">
                            Vous dirigez une équipe de soldats en charge de découvrir ce qui est arrivé aux scientifiques du laboratoire minier Omega Centuri B. Une fois à bord, vous apprenez qu'un terrible virus a fait muter tout l'équipage, et que la station spatiale
                        </p>
                        <div class="attraction-color attraction-color-2"></div>
                    </div>
                </div>
                <a href="index.php?page=attractions" class="btn btn-primary">Toutes les attractions</a>
            </div>
        </div>
    </section>
</main>