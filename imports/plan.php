<section id="plan">
    <div class="container">
        <div class="row">

            <div class="col-12">
                <h2>Le plan</h2>
            </div>
    
            <div class="col-xl-3 col-lg-4 col-md-12 col-sm-12">
                <div class="filtre">
                    <h3>Filtres</h3>

                    <!-- Les restaurants -->
                    <div class="checkbox-filtres">
                        <input type="checkbox" id="restaurants" class="custom-control-input" checked>
                        <label for="restaurants" class="custom-control-label">Les restaurants <img src="assets/img/plan/icon-violette.png" class="icon-marker" alt="markeur violet"></label>
                    </div>

                    <!-- Les services -->
                    <div class="checkbox-filtres">
                        <input type="checkbox" id="services" class="custom-control-input" checked>
                        <label for="services" class="custom-control-label">Les services <img src="assets/img/plan/icon-rose.png" class="icon-marker" alt="markeur rose"></label>
                    </div>

                    <!-- Les boutiques -->
                    <div class="checkbox-filtres">
                        <input type="checkbox" id="boutiques" class="custom-control-input" checked>
                        <label for="boutiques" class="custom-control-label">Les boutiques <img src="assets/img/plan/icon-bleu.png" class="icon-marker" alt="markeur bleu"></label>
                    </div>

                    <!-- Les points photo -->
                    <div class="checkbox-filtres">
                        <input type="checkbox" id="photos" class="custom-control-input" checked>
                        <label for="photos" class="custom-control-label">Les points photos <img src="assets/img/plan/icon-jaune.png" class="icon-marker" alt="markeur jaune"></label>
                    </div>

                    <!-- Les attractions -->
                    <div class="checkbox-filtres">
                        <input type="checkbox" id="attractions" class="custom-control-input" checked>
                        <label for="attractions" class="custom-control-label">Les attractions</label>
                    </div>

                    <!-- Les attractions -1m10 -->
                    <div class="checkbox-filtres">
                        <input type="checkbox" id="attractions110" class="custom-control-input">
                        <label for="attractions110" class="custom-control-label">Attractions disponibles aux moins d'1m10</label>
                    </div>

                    <!-- Les attractions -1m30 -->
                    <div class="checkbox-filtres">
                        <input type="checkbox" id="attractions130" class="custom-control-input">
                        <label for="attractions130" class="custom-control-label">Attractions disponibles aux moins d'1m30</label>
                    </div>

                </div>
            </div>
    
            <div class="col-xl-9 col-lg-8 col-md-12 col-sm-12">
                <div id="map"></div>
            </div>
        </div>
    </div>
</section>