<header>
    <div class="container">
        <div class="row padding-header" justify-content-between>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="logo-gamezone">
                    <a href="index.php?page=le-parc">
                        <img src="assets/img/logo gamezone.png" class="image-responsive" alt="logotype gamezone">
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 align-infos-user">
                <div class="infos-user">
                    <p class="pseudo"><a href="index.php?page=profil">Ellwye</a></p>
                    <p class="exp">0 exp</p>
                </div>
                <div class="round-avatar">
                </div>
                <div class="image-avatar">
                    <a href="index.php?page=profil"><img src="assets/img/avatars/avatar-loup.png" class="image-responsive" alt="avatar de l'utilisateur"></a>
                </div>
            </div>
        </div>
    </div>
</header>

<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav mr-auto mt-lg-0">
            <li class="nav-item active">
                <a class="nav-link" href="index.php?page=le-parc">Le parc</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?page=attractions">Les attractions</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?page=plan">Le plan</a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Preparer ma visite
                    </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Tarif et billeterie</a>
                    <a class="dropdown-item" href="#">Horaires et calendrier</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Se rendre au parc</a>
                    </div>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="index.php?page=game">My game</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?page=infos">Infos</a>
            </li>
        </ul>
    </div>
</nav>