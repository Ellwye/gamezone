<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Gamezone | Premier parc d'attraction entièrement dédié aux jeux-vidéo</title>

	<!-- Icons -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src='https://kit.fontawesome.com/a076d05399.js'></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Favicon -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/img/favicon.ico">
	<link rel="shortcut icon" href="assets/img/favicon.ico">

	<!-- Fonts import -->
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,400i,600,600i,700,700i&display=swap"
		rel="stylesheet">

	<!-- Styles imports -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.5/leaflet.css" />
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link href="assets/css/styles.min.css" type="text/css" rel="stylesheet">

	<!-- Global metas -->
	<meta name="description"
		content="GEEK CYBERCENTER est une SAS au capital de 45 824 123 euros. Il s’agit d’un groupe possédant plusieurs parcs d’attractions dans le
            monde comme : BATTLE KART VR, FORTNITE ADVENTURE, PUBG SURVIVOR. En septembre 2019, le groupe ouvrira un nouveau parc : GAME ZONE. Ce dernier sera le premier parc d’attraction au monde entièrement dédié à l’univers des jeux-vidéo.">
	<meta name="author" content="Gamezone">
	<meta name="keywords" content="parc d'attractions, jeux-vidéo">

	<!-- Metas Facebook & Twitter for sharing -->
	<meta property="og:url" content="https://www.gamezone.fr" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Gamezone" />
	<meta property="og:description"
		content="GEEK CYBERCENTER est une SAS au capital de 45 824 123 euros. Il s’agit d’un groupe possédant plusieurs parcs d’attractions dans le
            monde comme : BATTLE KART VR, FORTNITE ADVENTURE, PUBG SURVIVOR. En septembre 2019, le groupe ouvrira un nouveau parc : GAME ZONE. Ce dernier sera le premier parc d’attraction au monde entièrement dédié à l’univers des jeux-vidéo." />
	<meta property="og:image" content="assets/img/logotype-gamezone.png" />
	<meta name="twitter:card"
		content="GEEK CYBERCENTER est une SAS au capital de 45 824 123 euros. Il s’agit d’un groupe possédant plusieurs parcs d’attractions dans le
            monde comme : BATTLE KART VR, FORTNITE ADVENTURE, PUBG SURVIVOR. En septembre 2019, le groupe ouvrira un nouveau parc : GAME ZONE. Ce dernier sera le premier parc d’attraction au monde entièrement dédié à l’univers des jeux-vidéo." />
	<meta name="twitter:site" content="https://www.gamezone.fr" />
	<meta name="twitter:creator" content="Gamezone" />
</head>