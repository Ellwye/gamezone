<?php
    session_start();
    // include('class/autoload.php');
?>
<!DOCTYPE html>
<html lang="fr">
<?php
    require_once("imports/head.php");
?>
<body>
    <?php
        require_once("imports/header.php");
        if (isset($_GET["page"]) && file_exists("imports/".$_GET["page"].".php")) {
            require_once("imports/".$_GET["page"].".php");
        } else {
            require_once("imports/le-parc.php");
        }
        require_once("imports/footer.php");
        require_once("imports/scripts.php");
    ?>
</body>
</html>