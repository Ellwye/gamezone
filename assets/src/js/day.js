$(document).ready(function () {

    let day = new Date();
    let today = day.getDay();
    const days_of_week = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"];
    let day_today = days_of_week[today-1];



    if (today > 4) {
        if (today == 7) {
            //Le dimanche : 9h – 18h
            $("#day").html("<strong>Aujourd'hui, nous sommes " + day_today +  " : le parc est ouvert de 9h à 18h</strong>");
        } else {
            //Du vendredi au samedi : 9h-20h
            $("#day").html("<strong>Aujourd'hui, nous sommes " + day_today +  " : le parc est ouvert de 9h à 20h</strong>");
        }
    } else {
        //Du lundi au jeudi : 9h-19h
        $("#day").html("<strong>Aujourd'hui, nous sommes " + day_today +  " : le parc est ouvert de 9h à 19h</strong>");
    }
});