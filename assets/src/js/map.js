var map = L.map('map', {
    crs: L.CRS.Simple,
    minZoom: -1,
    scrollWheelZoom: true
});

var bounds = [[0,0], [1000,1000]];
var image = L.imageOverlay('assets/img/plan/plan_sansfond.png', bounds).addTo(map);
map.fitBounds(bounds);

var LeafIcon = L.Icon.extend({
    options: {
        iconSize:     [30, 46], // size of the icon
        iconAnchor:   [15, 46], // point of the icon which will correspond to marker's location
        popupAnchor:  [0, -45] // point from which the popup should open relative to the iconAnchor
    }
});

var purpleIcon = new LeafIcon({iconUrl: 'assets/img/plan/icon-violette.png'});
let resto_1 = L.marker([509, 170], {icon: purpleIcon}).bindPopup("<b>Restaurant 1</b>").addTo(map);
let resto_2 = L.marker([456, 377], {icon: purpleIcon}).bindPopup("<b>Restaurant 2</b>").addTo(map);
let resto_3 = L.marker([800, 548], {icon: purpleIcon}).bindPopup("<b>Restaurant 3</b>").addTo(map);
let resto_4 = L.marker([730, 757], {icon: purpleIcon}).bindPopup("<b>Restaurant 4</b>").addTo(map);
let resto_5 = L.marker([561, 712], {icon: purpleIcon}).bindPopup("<b>Restaurant 5</b>").addTo(map);

var pinkIcon = new LeafIcon({iconUrl: 'assets/img/plan/icon-rose.png'});
let service_1 = L.marker([669, 311], {icon: pinkIcon}).addTo(map).bindPopup("<b>Service 1</b>");
let service_2 = L.marker([585, 338], {icon: pinkIcon}).addTo(map).bindPopup("<b>Service 2</b>");
let service_3 = L.marker([496, 423], {icon: pinkIcon}).addTo(map).bindPopup("<b>Service 3</b>");
let service_4 = L.marker([511, 662], {icon: pinkIcon}).addTo(map).bindPopup("<b>Service 4</b>");
let service_5 = L.marker([642, 772], {icon: pinkIcon}).addTo(map).bindPopup("<b>Service 5</b>");

var blueIcon = new LeafIcon({iconUrl: 'assets/img/plan/icon-bleu.png'});
let boutique_1 = L.marker([400, 229], {icon: blueIcon}).addTo(map).bindPopup("<b>Boutique 1</b>");
let boutique_2 = L.marker([531, 461], {icon: blueIcon}).addTo(map).bindPopup("<b>Boutique 2</b>");
let boutique_3 = L.marker([679, 620], {icon: blueIcon}).addTo(map).bindPopup("<b>Boutique 3</b>");
let boutique_4 = L.marker([768, 624], {icon: blueIcon}).addTo(map).bindPopup("<b>Boutique 4</b>");
let boutique_5 = L.marker([593, 849], {icon: blueIcon}).addTo(map).bindPopup("<b>Boutique 5</b>");

var yellowIcon = new LeafIcon({iconUrl: 'assets/img/plan/icon-jaune.png'});
let photo_1 = L.marker([512, 290], {icon: yellowIcon}).addTo(map).bindPopup("<b>Point photo 1</b>");
let photo_2 = L.marker([667, 440], {icon: yellowIcon}).addTo(map).bindPopup("<b>Point photo 2</b>");
let photo_3 = L.marker([710, 565], {icon: yellowIcon}).addTo(map).bindPopup("<b>Point photo 3</b>");
let photo_4 = L.marker([618, 690], {icon: yellowIcon}).addTo(map).bindPopup("<b>Point photo 4</b>");
let photo_5 = L.marker([683, 861], {icon: yellowIcon}).addTo(map).bindPopup("<b>Point photo 5</b>");


var LeafAttraction = L.Icon.extend({
    options: {
        iconSize:     [150, 150], // size of the icon
        iconAnchor:   [75, 75], // point of the icon which will correspond to marker's location
        popupAnchor:  [0, -45] // point from which the popup should open relative to the iconAnchor
    }
});

var fighter = new LeafAttraction({iconUrl: 'assets/img/attractions/logo_fighter_hard_team.png'});
let fighter_attraction = L.marker([480, 190], {icon: fighter}).addTo(map).bindPopup("<b>Attraction Fighter hard team</b>");

var game_center = new LeafAttraction({iconUrl: 'assets/img/attractions/logo_game_center.png'});
let game_center_attraction = L.marker([556, 130], {icon: game_center}).addTo(map).bindPopup("<b>Attraction Game Center</b>");

var heroes = new LeafAttraction({iconUrl: 'assets/img/attractions/logo_awesome_heroes_team.png'});
let heroes_attraction = L.marker([595, 230], {icon: heroes}).addTo(map).bindPopup("<b>Attraction Heroes Team</b>");

var battle_kart = new LeafAttraction({iconUrl: 'assets/img/attractions/logo_BATTLE_KART.png'});
let battle_kart_attraction = L.marker([417, 461], {icon: battle_kart}).addTo(map).bindPopup("<b>Attraction Battle Kart</b>");

var champions_league = new LeafAttraction({iconUrl: 'assets/img/attractions/logo_champions_league.png'});
let champions_league_attraction = L.marker([479, 836], {icon: champions_league}).addTo(map).bindPopup("<b>Attraction Champions League</b>");

var heroes_team = new LeafAttraction({iconUrl: 'assets/img/attractions/logo_heroes_team.png'});
let heroes_team_attraction = L.marker([658, 889], {icon: heroes_team}).addTo(map).bindPopup("<b>Attraction Heroes Team</b>");

var fighter_league = new LeafAttraction({iconUrl: 'assets/img/attractions/logo_super_fighter_league.png'});
let fighter_league_attraction = L.marker([805, 689], {icon: fighter_league}).addTo(map).bindPopup("<b>Attraction Fighter League</b>");

var champion_league = new LeafAttraction({iconUrl: 'assets/img/attractions/logo_champions_league_survivor.png'});
let champion_league_attraction = L.marker([749, 381], {icon: champion_league}).addTo(map).bindPopup("<b>Attraction Champions League Survivor</b>");

var contagion_vr = new LeafAttraction({iconUrl: 'assets/img/attractions/logo_contagion_VR.png'});
let contagion_vr_attraction = L.marker([782, 852], {icon: contagion_vr}).addTo(map).bindPopup("<b>Attraction Contagion VR</b>");

map.setView( [500, 500], 0);

map.addEventListener("click", function(e){
    // Définition des variables contenant la latitude et la longitude
    let lati = e.latlng.lat;
    let long = e.latlng.lng;
    console.log(lati + "    " + long);
});



let filtre_restaurants = $("#restaurants");
filtre_restaurants.click(function () { 
    filtre_resto_statut = filtre_restaurants.prop("checked");
    // console.log(filtre_resto_statut);

    if (filtre_resto_statut) {
        resto_1.addTo(map);
        resto_2.addTo(map);
        resto_3.addTo(map);
        resto_4.addTo(map);
        resto_5.addTo(map);
    } else {
        map.removeLayer(resto_1);
        map.removeLayer(resto_2);
        map.removeLayer(resto_3);
        map.removeLayer(resto_4);
        map.removeLayer(resto_5);
    }
});

let filtre_attractions110 = $("#attractions110");
let filtre_attractions130 = $("#attractions130");
let filtre_attractions = $("#attractions");
let filtre_photos = $("#photos");
let filtre_boutiques = $("#boutiques");
let filtre_services = $("#services");

filtre_services.click(function () { 
    filtre_services_statut = filtre_services.prop("checked");
    // console.log(filtre_resto_statut);

    if (filtre_services_statut) {
        service_1.addTo(map);
        service_2.addTo(map);
        service_3.addTo(map);
        service_4.addTo(map);
        service_5.addTo(map);
    } else {
        map.removeLayer(service_1);
        map.removeLayer(service_2);
        map.removeLayer(service_3);
        map.removeLayer(service_4);
        map.removeLayer(service_5);
    }
});

filtre_boutiques.click(function () { 
    filtre_boutiques_statut = filtre_boutiques.prop("checked");
    // console.log(filtre_resto_statut);

    if (filtre_boutiques_statut) {
        boutique_1.addTo(map);
        boutique_2.addTo(map);
        boutique_3.addTo(map);
        boutique_4.addTo(map);
        boutique_5.addTo(map);
    } else {
        map.removeLayer(boutique_1);
        map.removeLayer(boutique_2);
        map.removeLayer(boutique_3);
        map.removeLayer(boutique_4);
        map.removeLayer(boutique_5);
    }
});

filtre_photos.click(function () { 
    filtre_photos_statut = filtre_photos.prop("checked");
    // console.log(filtre_resto_statut);

    if (filtre_photos_statut) {
        photo_1.addTo(map);
        photo_2.addTo(map);
        photo_3.addTo(map);
        photo_4.addTo(map);
        photo_5.addTo(map);
    } else {
        map.removeLayer(photo_1);
        map.removeLayer(photo_2);
        map.removeLayer(photo_3);
        map.removeLayer(photo_4);
        map.removeLayer(photo_5);
    }
});

filtre_attractions.click(function () { 
    filtre_attractions_statut = filtre_attractions.prop("checked");
    // console.log(filtre_resto_statut);

    if (filtre_attractions_statut) {
        fighter_attraction.addTo(map);
        game_center_attraction.addTo(map);
        heroes_attraction.addTo(map);
        battle_kart_attraction.addTo(map);
        champions_league_attraction.addTo(map);
        heroes_team_attraction.addTo(map);
        fighter_league_attraction.addTo(map);
        champion_league_attraction.addTo(map);
        contagion_vr_attraction.addTo(map);
    } else {
        map.removeLayer(fighter_attraction);
        map.removeLayer(game_center_attraction);
        map.removeLayer(heroes_attraction);
        map.removeLayer(battle_kart_attraction);
        map.removeLayer(champions_league_attraction);
        map.removeLayer(heroes_team_attraction);
        map.removeLayer(fighter_league_attraction);
        map.removeLayer(champion_league_attraction);
        map.removeLayer(contagion_vr_attraction);
    }
});

filtre_attractions130.click(function () { 
    filtre_attractions130_statut = filtre_attractions130.prop("checked");
    // console.log(filtre_resto_statut);

    if (filtre_attractions130_statut) {
        map.removeLayer(fighter_attraction);
        map.removeLayer(heroes_team_attraction);
        map.removeLayer(contagion_vr_attraction);
        
    } else {

        if (!filtre_attractions110.prop("checked")) {
            fighter_attraction.addTo(map);
            heroes_team_attraction.addTo(map);
            contagion_vr_attraction.addTo(map);
            champions_league_attraction.addTo(map);
            fighter_league_attraction.addTo(map);
        }
    }
});

filtre_attractions110.click(function () { 
    filtre_attractions110_statut = filtre_attractions110.prop("checked");
    // console.log(filtre_resto_statut);

    if (filtre_attractions110_statut) {
        map.removeLayer(fighter_attraction);
        map.removeLayer(heroes_team_attraction);
        map.removeLayer(contagion_vr_attraction);
        map.removeLayer(champions_league_attraction);
        map.removeLayer(fighter_league_attraction);
        
    } else {

        console.log(filtre_attractions130_statut);

        if (filtre_attractions130_statut) {
            fighter_attraction.addTo(map);
            heroes_team_attraction.addTo(map);
            contagion_vr_attraction.addTo(map);
        } else {
            fighter_attraction.addTo(map);
            heroes_team_attraction.addTo(map);
            contagion_vr_attraction.addTo(map);
            champions_league_attraction.addTo(map);
            fighter_league_attraction.addTo(map);
        }
    }
});